﻿

namespace Codefarts.Imaging.Tests
{
    using System;
    using System.Drawing;
    using System.IO;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Color = Codefarts.Imaging.Color;

    [TestClass]
    public class GenericImageTests
    {
        [TestMethod]
        public void GenericImage_Pack_Transparent_Black()
        {
            var color = new Color(0, 0, 0, 0);
            var packed = color.Pack();

            Assert.AreEqual(0u, packed);
        }

        [TestMethod]
        public void GenericImage_UnPack_Transparent_Black()
        {
            var packed = 0u;
            var color = packed.UnpackColor();

            Assert.AreEqual(new Color(0, 0, 0, 0), color);
        }

        [TestMethod]
        public void GenericImage_Pack_White()
        {
            var color = Color.White;
            var packed = color.Pack();

            Assert.AreEqual(uint.MaxValue, packed);
        }

        [TestMethod]
        public void GenericImage_UnPack_White()
        {
            var packed = uint.MaxValue;
            var color = packed.UnpackColor();

            Assert.AreEqual(Color.White, color);
        }

        [TestMethod]
        public void GenericImage_Load32BppRaw()
        {
            // create a 4x4 image and draw a red border around it
            var image = new GenericImage<Color>(4, 4);
            image.DrawRectangle(0, 0, 4, 4, Color.Red);

            // create a memory stream and save the image to it
            var memory = new MemoryStream();
            image.Save32BppRaw(memory);

            // set position back to beginning and read data from memory stream into data array
            memory.Seek(0, SeekOrigin.Begin);

            // create a initial image with different dimensions
            var loadedImage = new GenericImage<Color>(2, 1);
            loadedImage.Load32BppRaw(memory);

            // compare pixel data to see if it matches
            image.ForEach((genericImage, x, y) => Assert.AreEqual(genericImage[x, y], loadedImage[x, y]));
        }

        [TestMethod]
        public void GenericImage_Save32BppRaw()
        {
            // create a 4x4 image and draw a red border around it
            var image = new GenericImage<Color>(4, 4);
            image.DrawRectangle(0, 0, 3, 3, Color.Red);

            // create a memory stream and save the image to it
            var memory = new MemoryStream();
            image.Save32BppRaw(memory);

            // set position back to beginning and read data from memory stream into data array
            memory.Seek(0, SeekOrigin.Begin);
            var data = new byte[memory.Length];
            using (var reader = new BinaryReader(memory))
            {
                reader.Read(data, 0, data.Length);
            }

            // setup bytes for carrage return, line feed & comma
            var crlf = new[] { Convert.ToByte('\r'), Convert.ToByte('\n'), Convert.ToByte(',') };

            // setup a dimensions array and write/read the byte data for the image dimensions
            byte[] dimensions;
            using (var tempStream = new MemoryStream())
            {
                // write dimension data
                var writer = new BinaryWriter(tempStream);
                writer.Write(image.Width);
                writer.Flush();

                // read dimension data
                dimensions = new byte[tempStream.Length];
                tempStream.Seek(0, SeekOrigin.Begin);
                using (var reader = new BinaryReader(tempStream))
                {
                    reader.Read(dimensions, 0, dimensions.Length);
                }
            }

            // setup an array of expected by data to be returned
            var expected = new byte[]
                {
                    dimensions[0], dimensions[1], dimensions[2], dimensions[3],  crlf[0], crlf[1],
                    dimensions[0], dimensions[1], dimensions[2], dimensions[3],  crlf[0], crlf[1],
                    255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2], 255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2], 255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2], 255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2], crlf[0], crlf[1], 
                    255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2],   0, crlf[2],   0, crlf[2],  0, crlf[2], 0, crlf[2],   0, crlf[2],   0, crlf[2],  0, crlf[2], 0, crlf[2], 255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2], crlf[0], crlf[1],
                    255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2],   0, crlf[2],   0, crlf[2],  0, crlf[2], 0, crlf[2],   0, crlf[2],   0, crlf[2],  0, crlf[2], 0, crlf[2], 255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2], crlf[0], crlf[1],
                    255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2], 255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2], 255, crlf[2], 255, crlf[2],  0, crlf[2], 0, crlf[2], 255, crlf[2], 255, crlf[2],  0, crlf[2], 0 
                };

            // check if expected data matches the data that was read
            for (var i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(expected[i], data[i]);
            }
        }

        [TestMethod]
        public void GenericImage_Creation()
        {
            var img = new GenericImage<Color>(32, 32);

            // verify that all pixels are transparent
            foreach (var color in img.PixelGrid)
            {
                Assert.AreEqual(color, new Color());
            }
        }

        [TestMethod]
        public void GenericImage_Creation_Negative_Width()
        {
            try
            {
                var img = new GenericImage<Color>(-32, 32);
            }
            catch (System.Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(System.ArgumentOutOfRangeException));
                return;
            }

            Assert.Fail("Expected a ArgumentOutOfRangeException to be thrown!");
        }

        [TestMethod]
        public void GenericImage_Creation_Negative_Height()
        {
            try
            {
                var img = new GenericImage<Color>(32, -32);
            }
            catch (System.Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(System.ArgumentOutOfRangeException));
                return;
            }

            Assert.Fail("Expected a ArgumentOutOfRangeException to be thrown!");
        }

        [TestMethod]
        public void GenericImage_DrawRectangle()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the rectangle
            img.DrawRectangle(0, 0, 3, 3, Color.Red);

            // verify that all border pixels are red
            for (var i = 0; i < img.Width; i++)
            {
                // top/bottom
                Assert.AreEqual(img[i, 0], Color.Red);
                Assert.AreEqual(img[i, img.Height - 1], Color.Red);

                // left/right
                Assert.AreEqual(img[0, i], Color.Red);
                Assert.AreEqual(img[img.Width - 1, i], Color.Red);
            }

            // verify that center pixels are transparent
            Assert.AreEqual(img[1, 1], new Color());
            Assert.AreEqual(img[2, 1], new Color());
            Assert.AreEqual(img[1, 2], new Color());
            Assert.AreEqual(img[2, 2], new Color());
        }

        [TestMethod]
        public void GenericImage_FillRectangle()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the rectangle
            img.FillRectangle(0, 0, 3, 3, Color.Red);

            // verify that all pixels are red
            foreach (var color in img.PixelGrid)
            {
                Assert.AreEqual(color, Color.Red);
            }
        }

        [TestMethod]
        public void GenericImage_FillRectangle_CenterOfImage()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the rectangle
            img.FillRectangle(1, 1, 2, 2, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], new Color());
            Assert.AreEqual(img[1, 0], new Color());
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], new Color());

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], new Color());
            Assert.AreEqual(img[1, 1], Color.Red);
            Assert.AreEqual(img[2, 1], Color.Red);
            Assert.AreEqual(img[3, 1], new Color());

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], Color.Red);
            Assert.AreEqual(img[2, 2], Color.Red);
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], new Color());
            Assert.AreEqual(img[1, 3], new Color());
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], new Color());
        }

        [TestMethod]
        public void GenericImage_FillRectangle_Bottom_Right_Overhang()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the rectangle
            img.FillRectangle(2, 2, 4, 4, Color.Red);

            // verify that all border pixels are clear
            for (var i = 0; i < img.Width; i++)
            {
                // top/bottom
                Assert.AreEqual(img[i, 0], new Color());
                Assert.AreEqual(img[i, 1], new Color());

                // left/right
                Assert.AreEqual(img[0, i], new Color());
                Assert.AreEqual(img[1, i], new Color());
            }

            // verify that the bottom right 4 pixels are transparent
            Assert.AreEqual(img[2, 2], Color.Red);
            Assert.AreEqual(img[3, 2], Color.Red);
            Assert.AreEqual(img[2, 3], Color.Red);
            Assert.AreEqual(img[3, 3], Color.Red);
        }

        [TestMethod]
        public void GenericImage_FillRectangle_Top_Left_Overhang()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the rectangle
            img.FillRectangle(-2, -2, 4, 4, Color.Red);

            // verify that all border pixels are clear
            for (var i = 0; i < img.Width; i++)
            {
                // top/bottom
                Assert.AreEqual(img[i, img.Height - 1], new Color());
                Assert.AreEqual(img[i, img.Height - 2], new Color());

                // left/right
                Assert.AreEqual(img[img.Width - 1, i], new Color());
                Assert.AreEqual(img[img.Width - 2, i], new Color());
            }

            // verify that the top right 4 pixels are transparent
            Assert.AreEqual(img[0, 0], Color.Red);
            Assert.AreEqual(img[1, 0], Color.Red);
            Assert.AreEqual(img[0, 1], Color.Red);
            Assert.AreEqual(img[1, 1], Color.Red);
        }

        [TestMethod]
        public void GenericImage_DrawLine_Border()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the top/bottom lines full width
            img.DrawLine(0, 0, 3, 0, Color.Red);
            img.DrawLine(0, 3, 3, 3, Color.Red);

            // draw the left/right lines full height minus two
            img.DrawLine(0, 1, 0, 2, Color.Red);
            img.DrawLine(3, 1, 3, 2, Color.Red);

            // verify that all border pixels are red
            for (int i = 0; i < img.Width; i++)
            {
                // top/bottom
                Assert.AreEqual(img[i, 0], Color.Red);
                Assert.AreEqual(img[i, img.Height - 1], Color.Red);

                // left/right
                Assert.AreEqual(img[0, i], Color.Red);
                Assert.AreEqual(img[img.Width - 1, i], Color.Red);
            }

            // verify that center pixels are transparent
            Assert.AreEqual(img[1, 1], new Color());
            Assert.AreEqual(img[2, 1], new Color());
            Assert.AreEqual(img[1, 2], new Color());
            Assert.AreEqual(img[2, 2], new Color());
        }

        [TestMethod]
        public void GenericImage_DrawLine_TopLeft_To_BottomRight()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the line
            img.DrawLine(0, 0, 3, 3, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], Color.Red);
            Assert.AreEqual(img[1, 0], new Color());
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], new Color());

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], new Color());
            Assert.AreEqual(img[1, 1], Color.Red);
            Assert.AreEqual(img[2, 1], new Color());
            Assert.AreEqual(img[3, 1], new Color());

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], new Color());
            Assert.AreEqual(img[2, 2], Color.Red);
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], new Color());
            Assert.AreEqual(img[1, 3], new Color());
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], Color.Red);
        }

        [TestMethod]
        public void GenericImage_DrawLine_TopRight_To_BottomLeft()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the line
            img.DrawLine(3, 0, 0, 3, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], new Color());
            Assert.AreEqual(img[1, 0], new Color());
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], Color.Red);

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], new Color());
            Assert.AreEqual(img[1, 1], new Color());
            Assert.AreEqual(img[2, 1], Color.Red);
            Assert.AreEqual(img[3, 1], new Color());

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], Color.Red);
            Assert.AreEqual(img[2, 2], new Color());
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], Color.Red);
            Assert.AreEqual(img[1, 3], new Color());
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], new Color());
        }

        [TestMethod]
        public void GenericImage_DrawLine_BottomRight_To_TopLeft()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the line
            img.DrawLine(3, 3, 0, 0, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], Color.Red);
            Assert.AreEqual(img[1, 0], new Color());
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], new Color());

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], new Color());
            Assert.AreEqual(img[1, 1], Color.Red);
            Assert.AreEqual(img[2, 1], new Color());
            Assert.AreEqual(img[3, 1], new Color());

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], new Color());
            Assert.AreEqual(img[2, 2], Color.Red);
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], new Color());
            Assert.AreEqual(img[1, 3], new Color());
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], Color.Red);
        }

        [TestMethod]
        public void GenericImage_DrawLine_BottomLeft_To_TopRight()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the line
            img.DrawLine(0, 3, 3, 0, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], new Color());
            Assert.AreEqual(img[1, 0], new Color());
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], Color.Red);

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], new Color());
            Assert.AreEqual(img[1, 1], new Color());
            Assert.AreEqual(img[2, 1], Color.Red);
            Assert.AreEqual(img[3, 1], new Color());

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], Color.Red);
            Assert.AreEqual(img[2, 2], new Color());
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], Color.Red);
            Assert.AreEqual(img[1, 3], new Color());
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], new Color());
        }

        [TestMethod]
        public void GenericImage_DrawLine_X()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the X lines
            img.DrawLine(0, 0, 3, 3, Color.Red);
            img.DrawLine(3, 0, 0, 3, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], Color.Red);
            Assert.AreEqual(img[1, 0], new Color());
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], Color.Red);

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], new Color());
            Assert.AreEqual(img[1, 1], Color.Red);
            Assert.AreEqual(img[2, 1], Color.Red);
            Assert.AreEqual(img[3, 1], new Color());

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], Color.Red);
            Assert.AreEqual(img[2, 2], Color.Red);
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], Color.Red);
            Assert.AreEqual(img[1, 3], new Color());
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], Color.Red);
        }

        [TestMethod]
        public void GenericImage_DrawRectangle_Border_ToByteArray()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the X lines
            img.DrawRectangle(0, 0, 3, 3, Color.Red);
            var data = img.ToByteArray();

            var compare = new byte[] 
                { 
                    255, 0, 0, 255,      255,   0,   0, 255,      255,   0,   0, 255,     255, 0, 0, 255, 
                    255, 0, 0, 255,      0, 0, 0, 0,              0, 0, 0, 0,             255, 0, 0, 255,  
                    255, 0, 0, 255,      0, 0, 0, 0,              0, 0, 0, 0,             255, 0, 0, 255, 
                    255, 0, 0, 255,      255,   0,   0, 255,      255,   0,   0, 255,     255, 0, 0, 255 
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (int i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }

        [TestMethod]
        public void GenericImage_DrawRectangle_Border_ToByteArray_Inner_Rect()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the X lines
            img.DrawRectangle(0, 0, 4, 4, Color.Red);
            var data = img.ToByteArray(1, 1, 2, 2);

            var compare = new byte[] 
                { 
                    0, 0, 0, 0,          0, 0, 0, 0,    
                    0, 0, 0, 0,          0, 0, 0, 0,    
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (int i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }

        [TestMethod]
        public void GenericImage_DrawRectangle_Border_ToByteArray_TopLeft_Rect()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the X lines
            img.DrawRectangle(0, 0, 4, 4, Color.Red);
            var data = img.ToByteArray(0, 0, 2, 2);

            var compare = new byte[] 
                { 
                    255, 0, 0, 255,          255, 0, 0, 255,    
                    255, 0, 0, 255,          0, 0, 0, 0,    
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (int i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }

        [TestMethod]
        public void GenericImage_DrawRectangle_Border_ToByteArray_BottomRight_Rect()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the X lines
            img.DrawRectangle(0, 0, 3, 3, Color.Red);
            var data = img.ToByteArray(2, 2, 2, 2);

            var compare = new byte[] 
                { 
                    0, 0, 0, 0,                    255, 0, 0, 255,     
                    255, 0, 0, 255,                255, 0, 0, 255,       
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (int i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }


        [TestMethod]
        public void GenericImage_DrawLine_Center_To_Beyond_Left()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.DrawLine(1, 1, -2, 1, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], new Color());
            Assert.AreEqual(img[1, 0], new Color());
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], new Color());

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], Color.Red);
            Assert.AreEqual(img[1, 1], Color.Red);
            Assert.AreEqual(img[2, 1], new Color());
            Assert.AreEqual(img[3, 1], new Color());

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], new Color());
            Assert.AreEqual(img[2, 2], new Color());
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], new Color());
            Assert.AreEqual(img[1, 3], new Color());
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], new Color());
        }

        [TestMethod]
        public void GenericImage_DrawLine_Center_To_Beyond_Top()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.DrawLine(1, 1, 1, -2, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], new Color());
            Assert.AreEqual(img[1, 0], Color.Red);
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], new Color());

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], new Color());
            Assert.AreEqual(img[1, 1], Color.Red);
            Assert.AreEqual(img[2, 1], new Color());
            Assert.AreEqual(img[3, 1], new Color());

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], new Color());
            Assert.AreEqual(img[2, 2], new Color());
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], new Color());
            Assert.AreEqual(img[1, 3], new Color());
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], new Color());
        }

        [TestMethod]
        public void GenericImage_DrawLine_Center_To_Beyond_Bottom()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.DrawLine(1, 1, 1, 6, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], new Color());
            Assert.AreEqual(img[1, 0], new Color());
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], new Color());

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], new Color());
            Assert.AreEqual(img[1, 1], Color.Red);
            Assert.AreEqual(img[2, 1], new Color());
            Assert.AreEqual(img[3, 1], new Color());

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], Color.Red);
            Assert.AreEqual(img[2, 2], new Color());
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], new Color());
            Assert.AreEqual(img[1, 3], Color.Red);
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], new Color());
        }

        [TestMethod]
        public void GenericImage_DrawLine_Center_To_Beyond_Right()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.DrawLine(1, 1, 6, 1, Color.Red);

            // verify that row 0 pixels are correct
            Assert.AreEqual(img[0, 0], new Color());
            Assert.AreEqual(img[1, 0], new Color());
            Assert.AreEqual(img[2, 0], new Color());
            Assert.AreEqual(img[3, 0], new Color());

            // verify that row 1 pixels are correct
            Assert.AreEqual(img[0, 1], new Color());
            Assert.AreEqual(img[1, 1], Color.Red);
            Assert.AreEqual(img[2, 1], Color.Red);
            Assert.AreEqual(img[3, 1], Color.Red);

            // verify that row 2 pixels are correct
            Assert.AreEqual(img[0, 2], new Color());
            Assert.AreEqual(img[1, 2], new Color());
            Assert.AreEqual(img[2, 2], new Color());
            Assert.AreEqual(img[3, 2], new Color());

            // verify that row 3 pixels are correct
            Assert.AreEqual(img[0, 3], new Color());
            Assert.AreEqual(img[1, 3], new Color());
            Assert.AreEqual(img[2, 3], new Color());
            Assert.AreEqual(img[3, 3], new Color());
        }

        [TestMethod]
        public void GenericImage_Draw()
        {
            var img = new GenericImage<Color>(4, 4);
            var destImage = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.DrawRectangle(0, 0, 3, 3, Color.Red);
            destImage.Draw(img, 0, 0, (sourceColor, blendWithColor) => sourceColor.Blend(blendWithColor));
            var data = destImage.ToByteArray();

            var compare = new byte[] 
                { 
                    255, 0, 0, 255,      255, 0, 0, 255,      255, 0, 0, 255,     255, 0, 0, 255, 
                    255, 0, 0, 255,      0, 0, 0, 0,          0, 0, 0, 0,         255, 0, 0, 255,  
                    255, 0, 0, 255,      0, 0, 0, 0,          0, 0, 0, 0,         255, 0, 0, 255, 
                    255, 0, 0, 255,      255, 0, 0, 255,      255, 0, 0, 255,     255, 0, 0, 255 
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (var i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }

        [TestMethod]
        public void GenericImage_Draw_From_Large_Source_Image_BlackTile()
        {
            // load image 
            var bmp = new Bitmap(this.GetType().Assembly.GetManifestResourceStream("Codefarts.Imaging.Tests.Resources.TilesSmall.png"));
            Assert.IsNotNull(bmp);

            var lockBmp = new LockBitmap(bmp);
            lockBmp.LockBits();

            // convert to GenericImage<Color>
            var img = new GenericImage<Color>(bmp.Width, bmp.Height);
            for (int idy = 0; idy < bmp.Height; idy++)
            {
                for (int idx = 0; idx < bmp.Width; idx++)
                {
                    var pixel = lockBmp.GetPixel(idx, idy);
                    img[idx, idy] = new Color(pixel.R, pixel.G, pixel.B, pixel.A);
                }
            }

            var destImage = new GenericImage<Color>(32, 32);
            destImage.Draw(img, 0, 0, 32, 32, 96, 0, 32, 32, (sourceColor, blendWithColor) => sourceColor.Blend(blendWithColor));

            foreach (var color in destImage.PixelGrid)
            {
                Assert.AreEqual(color, Color.Black);
            }

            //for (int idy = 0; idy < destImage.Height; idy++)
            //{
            //    for (int idx = 0; idx < destImage.Width; idx++)
            //    {
            //        var pixel = lockBmp.GetPixel(idx + 128, idy + 0);
            //        Assert.AreEqual(destImage[idx, idy], new Color(pixel.R, pixel.G, pixel.B, pixel.A));
            //    }
            //}

            lockBmp.UnlockBits();
        }                                                  

        [TestMethod]
        public void GenericImage_Draw_From_Large_Source_Image_PotTile()
        {
            // load image 
            var bmp = new Bitmap(this.GetType().Assembly.GetManifestResourceStream("Codefarts.Imaging.Tests.Resources.TilesSmall.png"));
            Assert.IsNotNull(bmp);

            var lockBmp = new LockBitmap(bmp);
            lockBmp.LockBits();

            // convert to GenericImage<Color>
            var img = new GenericImage<Color>(bmp.Width, bmp.Height);
            for (int idy = 0; idy < bmp.Height; idy++)
            {
                for (int idx = 0; idx < bmp.Width; idx++)
                {
                    var pixel = lockBmp.GetPixel(idx, idy);
                    img[idx, idy] = new Color(pixel.R, pixel.G, pixel.B, pixel.A);
                }
            }

            var destImage = new GenericImage<Color>(32, 32);
            destImage.Draw(img, 0, 0, 32, 32, 64, 0, 32, 32, (sourceColor, blendWithColor) => sourceColor.Blend(blendWithColor));

            for (int idy = 0; idy < destImage.Height; idy++)
            {
                for (int idx = 0; idx < destImage.Width; idx++)
                {
                    var pixel = lockBmp.GetPixel(idx + 64, idy + 0);
                    Assert.AreEqual(destImage[idx, idy], new Color(pixel.R, pixel.G, pixel.B, pixel.A));
                }
            }

            lockBmp.UnlockBits();
        }

        [TestMethod]
        public void GenericImage_Draw_From_Large_Source_Image_SignTile()
        {
            // load image 
            var bmp = new Bitmap(this.GetType().Assembly.GetManifestResourceStream("Codefarts.Imaging.Tests.Resources.TilesSmall.png"));
            Assert.IsNotNull(bmp);

            var lockBmp = new LockBitmap(bmp);
            lockBmp.LockBits();

            // convert to GenericImage<Color>
            var img = new GenericImage<Color>(bmp.Width, bmp.Height);
            for (int idy = 0; idy < bmp.Height; idy++)
            {
                for (int idx = 0; idx < bmp.Width; idx++)
                {
                    var pixel = lockBmp.GetPixel(idx, idy);
                    img[idx, idy] = new Color(pixel.R, pixel.G, pixel.B, pixel.A);
                }
            }

            var destImage = new GenericImage<Color>(32, 32);
            destImage.Draw(img, 0, 0, 32, 32, 192, 0, 32, 32, (sourceColor, blendWithColor) => sourceColor.Blend(blendWithColor));

            for (int idy = 0; idy < destImage.Height; idy++)
            {
                for (int idx = 0; idx < destImage.Width; idx++)
                {
                    var pixel = lockBmp.GetPixel(idx + 192, idy + 0);
                    Assert.AreEqual(destImage[idx, idy], new Color(pixel.R, pixel.G, pixel.B, pixel.A));
                }
            }

            lockBmp.UnlockBits();
        }

        [TestMethod]
        public void GenericImage_Draw_At_Center_Point()
        {
            var img = new GenericImage<Color>(4, 4);
            var destImage = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.DrawRectangle(0, 0, 4, 4, Color.Red);
            destImage.Draw(img, 2, 2, (sourceColor, blendWithColor) => sourceColor.Blend(blendWithColor));
            var data = destImage.ToByteArray();

            var compare = new byte[] 
                { 
                    0, 0, 0, 0,      0, 0, 0, 0,      0, 0, 0, 0,          0, 0, 0, 0, 
                    0, 0, 0, 0,      0, 0, 0, 0,      0, 0, 0, 0,          0, 0, 0, 0, 
                    0, 0, 0, 0,      0, 0, 0, 0,      255, 0, 0, 255,      255, 0, 0, 255, 
                    0, 0, 0, 0,      0, 0, 0, 0,      255, 0, 0, 255,      0, 0, 0, 0
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (var i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }

        [TestMethod]
        public void GenericImage_Draw_Negative_Half_Image_Size()
        {
            var img = new GenericImage<Color>(4, 4);
            var destImage = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.DrawRectangle(0, 0, 3, 3, Color.Red);
            destImage.Draw(img, -2, -2, (sourceColor, blendWithColor) => sourceColor.Blend(blendWithColor));
            var data = destImage.ToByteArray();

            var compare = new byte[] 
                { 
                    0, 0, 0, 0,            255, 0, 0, 255,        0, 0, 0, 0,     0, 0, 0, 0, 
                    255, 0, 0, 255,        255, 0, 0, 255,        0, 0, 0, 0,     0, 0, 0, 0, 
                    0, 0, 0, 0,            0, 0, 0, 0,            0, 0, 0, 0,     0, 0, 0, 0, 
                    0, 0, 0, 0,            0, 0, 0, 0,            0, 0, 0, 0,     0, 0, 0, 0
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (var i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }

        [TestMethod]
        public void GenericImage_FillRectangle_Center()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.FillRectangle(1, 1, 2, 2, Color.Red);
            var data = img.ToByteArray();

            var compare = new byte[] 
                { 
                    0, 0, 0, 0,   0, 0, 0, 0,         0, 0, 0, 0,         0, 0, 0, 0, 
                    0, 0, 0, 0,   255, 0, 0, 255,     255, 0, 0, 255,     0, 0, 0, 0,
                    0, 0, 0, 0,   255, 0, 0, 255,     255, 0, 0, 255,     0, 0, 0, 0, 
                    0, 0, 0, 0,   0, 0, 0, 0,         0, 0, 0, 0,         0, 0, 0, 0
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (var i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }

        [TestMethod]
        public void GenericImage_Fill_Center_Draw_With_All_Params()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.FillRectangle(1, 1, 2, 2, Color.Red);
            img.Draw(img, 2, 2, 2, 2, 1, 1, 2, 2, (sourceColor, blendWithColor) => sourceColor.Blend(blendWithColor));
            var data = img.ToByteArray();

            var compare = new byte[] 
                { 
                    0, 0, 0, 0,   0, 0, 0, 0,             0, 0, 0, 0,       0, 0, 0, 0, 
                    0, 0, 0, 0,   255, 0, 0, 255,         255, 0, 0, 255,         0, 0, 0, 0,
                    0, 0, 0, 0,   255, 0, 0, 255,         255, 0, 0, 255,         255, 0, 0, 255, 
                    0, 0, 0, 0,   0, 0, 0, 0,             255, 0, 0, 255,         255, 0, 0, 255
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (var i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }

        [TestMethod]
        public void GenericImage_Set_TopLeft_Pixel_Then_DrawStretch_Whole_Image()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the border lines
            img[0, 0] = Color.Red;
            img.Draw(img, 0, 0, 4, 4, 0, 0, 1, 1, (sourceColor, blendWithColor) => sourceColor.Blend(blendWithColor));
            var data = img.ToByteArray();

            var compare = new byte[] 
                { 
                    255, 0, 0, 255,   255, 0, 0, 255,  255, 0, 0, 255,   255, 0, 0, 255, 
                    255, 0, 0, 255,   255, 0, 0, 255,  255, 0, 0, 255,   255, 0, 0, 255,
                    255, 0, 0, 255,   255, 0, 0, 255,  255, 0, 0, 255,   255, 0, 0, 255, 
                    255, 0, 0, 255,   255, 0, 0, 255,  255, 0, 0, 255,   255, 0, 0, 255
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (int i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }

        [TestMethod]
        public void GenericImage_Scale()
        {
            var img = new GenericImage<Color>(4, 4);

            // draw the border lines
            img.DrawRectangle(0, 0, 3, 3, Color.Red);
            var destImage = img.Scale(2, 2);
            var data = destImage.ToByteArray();

            var compare = new byte[] 
                { 
                    255, 0, 0, 255,  255, 0, 0, 255,  255, 0, 0, 255,    255, 0, 0, 255,    255, 0, 0, 255,   255, 0, 0, 255,   255, 0, 0, 255,   255, 0, 0, 255,
                    255, 0, 0, 255,  255, 0, 0, 255,  255, 0, 0, 255,    255, 0, 0, 255,    255, 0, 0, 255,   255, 0, 0, 255,   255, 0, 0, 255,   255, 0, 0, 255,
                    255, 0, 0, 255,  255, 0, 0, 255,  0, 0, 0, 0,        0, 0, 0, 0,        0, 0, 0, 0,       0, 0, 0, 0,       255, 0, 0, 255,   255, 0, 0, 255,
                    255, 0, 0, 255,  255, 0, 0, 255,  0, 0, 0, 0,        0, 0, 0, 0,        0, 0, 0, 0,       0, 0, 0, 0,       255, 0, 0, 255,   255, 0, 0, 255,
                    255, 0, 0, 255,  255, 0, 0, 255,  0, 0, 0, 0,        0, 0, 0, 0,        0, 0, 0, 0,       0, 0, 0, 0,       255, 0, 0, 255,   255, 0, 0, 255,
                    255, 0, 0, 255,  255, 0, 0, 255,  0, 0, 0, 0,        0, 0, 0, 0,        0, 0, 0, 0,       0, 0, 0, 0,       255, 0, 0, 255,   255, 0, 0, 255,
                    255, 0, 0, 255,  255, 0, 0, 255,  255, 0, 0, 255,    255, 0, 0, 255,    255, 0, 0, 255,   255, 0, 0, 255,   255, 0, 0, 255,   255, 0, 0, 255,
                    255, 0, 0, 255,  255, 0, 0, 255,  255, 0, 0, 255,    255, 0, 0, 255,    255, 0, 0, 255,   255, 0, 0, 255,   255, 0, 0, 255,   255, 0, 0, 255
                };

            // compare lengths
            Assert.AreEqual(data.Length, compare.Length);

            // compare bytes
            for (var i = 0; i < data.Length; i++)
            {
                Assert.AreEqual(data[i], compare[i]);
            }
        }
    }
}
