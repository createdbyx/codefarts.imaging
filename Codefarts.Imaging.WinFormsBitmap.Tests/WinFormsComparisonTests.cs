﻿namespace Codefarts.Imaging.WinFormsBitmap.Tests
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Color = Codefarts.Imaging.Color;

    [TestClass]
    public class WinFormsComparisonTests
    {
        private GenericImage<Color> image;

        private Bitmap windowsImage;

        private bool previewComparisons = true;

        [TestInitialize]
        public void Setup()
        {
            this.image = new GenericImage<Color>(128, 128);
            //  var transparent = Color.Transparent;
            // this.image.Clear(transparent);
            this.windowsImage = this.image.ToBitmap();
            //this.windowsImage = new Bitmap(this.image.Width, this.image.Height, PixelFormat.Format32bppArgb);

            //using (var graphics = Graphics.FromImage(this.windowsImage))
            //{
            //    //graphics.CompositingQuality = CompositingQuality.AssumeLinear;
            //    graphics.SmoothingMode = SmoothingMode.None;
            //    graphics.Clear(System.Drawing.Color.FromArgb(transparent.A, transparent.R, transparent.G, transparent.B));
            //}
        }

        [TestMethod]
        public void InitialState_Comparison()
        {
            this.ComparePixels();
        }

        private void ComparePixels()
        {
            var lockBmp = new LockBitmap(this.windowsImage);
            try
            {
                lockBmp.LockBits();

                for (var idy = 0; idy < this.windowsImage.Height; idy++)
                {
                    for (var idx = 0; idx < this.windowsImage.Width; idx++)
                    {
                        var pixel = lockBmp.GetPixel(idx, idy);
                        var expected = this.image[idx, idy];
                        Assert.AreEqual(expected.A, pixel.A);
                        Assert.AreEqual(expected.R, pixel.R);
                        Assert.AreEqual(expected.G, pixel.G);
                        Assert.AreEqual(expected.B, pixel.B);
                    }
                }
            }
            finally
            {
                lockBmp.UnlockBits();
            }
        }

        [TestMethod]
        public void RectangleTest()
        {
            using (var graphics = Graphics.FromImage(this.windowsImage))
            {
                //graphics.CompositingQuality = CompositingQuality.AssumeLinear;
                graphics.SmoothingMode = SmoothingMode.None;
                graphics.DrawRectangle(Pens.Black, 0, 0, this.windowsImage.Width - 1, this.windowsImage.Height - 1);
            }

            this.image.DrawRectangle(0, 0, this.image.Width - 1, this.image.Height - 1, Color.Black);

            this.GenerateComparisons();
            this.ComparePixels();
        }

        [TestMethod]
        public void FillEllipse_Comparison()
        {
            using (var graphics = Graphics.FromImage(this.windowsImage))
            {
                //graphics.CompositingQuality = CompositingQuality.AssumeLinear;
                //  graphics.SmoothingMode = SmoothingMode.None;
                graphics.Clear(System.Drawing.Color.White);
                graphics.FillEllipse(Brushes.Black, 0, 0, this.windowsImage.Width - 1, this.windowsImage.Height - 1);
            }

            this.image.Clear(Color.White);
            this.image.FillEllipse(0, 0, this.image.Width - 1, this.image.Height - 1, Color.Black);

            this.GenerateComparisons();

            this.ComparePixels();
        }

        [TestMethod]
        public void Bitmap_ToGenericImage_Comparison()
        {
            // load image 
            var bmp = new Bitmap(this.GetType().Assembly.GetManifestResourceStream("Codefarts.Imaging.WinFormsBitmap.Tests.Resources.TilesSmall.png"));
            Assert.IsNotNull(bmp);

            this.windowsImage = bmp;
            this.image = this.windowsImage.ToGenericImage();

            this.GenerateComparisons();

            this.ComparePixels();
        }

        [TestMethod]
        public void GenericImage_ToBitmap_Comparison()
        {
            this.image = new GenericImage<Color>(128, 128);
            this.image.DrawRectangle(0, 0, 127, 127, Color.Red);
            this.image.DrawLine(0, 0, 127, 127, Color.Red);
            this.image.DrawLine(127, 0, 0, 127, Color.Red);

            this.windowsImage = this.image.ToBitmap();

            this.GenerateComparisons();

            this.ComparePixels();
        }

        [TestMethod]
        public void GenericImage_ReadFromPPM()
        {
            this.image.ReadFromPPM(this.GetType().Assembly.GetManifestResourceStream("Codefarts.Imaging.WinFormsBitmap.Tests.Resources.TilesSmall.ppm"));
            this.GenerateComparisons();
        }

        private void GenerateComparisons()
        {
            if (this.previewComparisons)
            {
                var bitmap = new Bitmap(this.image.Width * 2, this.image.Height * 2, PixelFormat.Format32bppArgb);
                var nonRelativeComparisonImage = this.image.BuildAlphaComparison<Color>(this.windowsImage.ToGenericImage(), false);
                var relativeComparisonImage = this.image.BuildAlphaComparison<Color>(this.windowsImage.ToGenericImage(), true, Color.Black);
                using (var graphics = Graphics.FromImage(bitmap))
                {
                    graphics.SmoothingMode = SmoothingMode.None;
                    graphics.DrawImageUnscaled(this.windowsImage, 0, 0);
                    graphics.DrawImageUnscaled(this.image.ToBitmap(), this.image.Width * 2, 0);
                    graphics.DrawImageUnscaled(relativeComparisonImage.ToBitmap(), 0, this.image.Height * 2);
                    graphics.DrawImageUnscaled(nonRelativeComparisonImage.ToBitmap(), this.image.Width * 2, this.image.Height * 2);
                }

                var tempFile = Path.ChangeExtension(Path.GetTempFileName(), ".png");
                bitmap.Save(tempFile, ImageFormat.Png);
                Process.Start(tempFile);

                //tempFile = Path.ChangeExtension(Path.GetTempFileName(), ".ppm");
                //using (var stream = new FileStream(tempFile, FileMode.CreateNew))
                //{
                //    bitmap.ToGenericImage().SaveToPPM(stream);
                //}
                //Process.Start(tempFile);

                tempFile = Path.ChangeExtension(Path.GetTempFileName(), ".image.ppm");
                using (var stream = new FileStream(tempFile, FileMode.CreateNew))
                {
                    this.image.SaveToPPM(stream);
                }
                Process.Start(tempFile);

                tempFile = Path.ChangeExtension(Path.GetTempFileName(), ".winimage.ppm");
                using (var stream = new FileStream(tempFile, FileMode.CreateNew))
                {
                    this.windowsImage.ToGenericImage().SaveToPPM(stream);
                }
                Process.Start(tempFile);

                tempFile = Path.ChangeExtension(Path.GetTempFileName(), ".nonRelativeComparisonImage.ppm");
                using (var stream = new FileStream(tempFile, FileMode.CreateNew))
                {
                    nonRelativeComparisonImage.SaveToPPM(stream);
                }
                Process.Start(tempFile);

                tempFile = Path.ChangeExtension(Path.GetTempFileName(), ".relativeComparisonImage.ppm");
                using (var stream = new FileStream(tempFile, FileMode.CreateNew))
                {
                    relativeComparisonImage.SaveToPPM(stream);
                }
                Process.Start(tempFile);

                //var tempFile = Path.ChangeExtension(Path.GetTempFileName(), "winform.png");
                //this.windowsImage.Save(tempFile, ImageFormat.Png);
                //Process.Start(tempFile);

                //tempFile = Path.ChangeExtension(Path.GetTempFileName(), "image.png");
                //this.image.ToBitmap().Save(tempFile, ImageFormat.Png);
                //Process.Start(tempFile);

                //tempFile = Path.ChangeExtension(Path.GetTempFileName(), "nonrelative.png");
                //var nonRelativeComparisonImage = this.image.BuildAlphaComparison<Color>(this.windowsImage.ToGenericImage(), false);
                //nonRelativeComparisonImage.ToBitmap().Save(tempFile, ImageFormat.Png);
                //Process.Start(tempFile);

                //tempFile = Path.ChangeExtension(Path.GetTempFileName(), "relative.png");
                //var relativeComparisonImage = this.image.BuildAlphaComparison<Color>(this.windowsImage.ToGenericImage(), true, Color.Black);
                //relativeComparisonImage.ToBitmap().Save(tempFile, ImageFormat.Png);
                //Process.Start(tempFile);
            }
        }

        [TestMethod]
        public void XTest()
        {
            using (var graphics = Graphics.FromImage(this.windowsImage))
            {
                //graphics.CompositingQuality = CompositingQuality.AssumeLinear;
                //  graphics.SmoothingMode = SmoothingMode.None;
                graphics.DrawLine(Pens.Black, 0, 0, this.windowsImage.Width - 1, this.windowsImage.Height - 1);
                graphics.DrawLine(Pens.Black, this.windowsImage.Width - 1, 0, 0, this.windowsImage.Height - 1);
            }

            this.image.DrawLine(0, 0, this.image.Width - 1, this.image.Height - 1, Color.Black);
            this.image.DrawLine(this.image.Width - 1, 0, 0, this.image.Height - 1, Color.Black);

            this.GenerateComparisons();
            this.ComparePixels();
        }

        [TestMethod]
        public void FullCircleTest()
        {
            float Deg2Rad = 0.01745329f;
            var halfWidth = this.image.Width / 2;
            var halfHeight = this.image.Height / 2;

            using (var graphics = Graphics.FromImage(this.windowsImage))
            {
                //graphics.CompositingQuality = CompositingQuality.AssumeLinear;
                graphics.SmoothingMode = SmoothingMode.None;
                for (var i = 0; i < 360; i++)
                {
                    var posX = halfWidth + (Math.Cos(i * Deg2Rad) * halfWidth);
                    var posY = halfHeight + (Math.Sin(i * Deg2Rad) * halfHeight);
                    graphics.DrawLine(Pens.Black, halfWidth, halfHeight, (int)posX, (int)posY);
                    this.image.DrawLine(halfWidth, halfHeight, (int)posX, (int)posY, Color.Black);
                }
            }

            this.GenerateComparisons();

            this.ComparePixels();
        }

        [TestMethod]
        public void FieldOfViewTest()
        {
            float Deg2Rad = 0.01745329f;
            var halfWidth = this.image.Width / 2;
            var halfHeight = this.image.Height / 2;

            using (var graphics = Graphics.FromImage(this.windowsImage))
            {
                //graphics.CompositingQuality = CompositingQuality.AssumeLinear;
                graphics.SmoothingMode = SmoothingMode.None;
                var stepSize = (60 * Deg2Rad) / 800;
                var i = 240 * Deg2Rad;
                var end = 300 * Deg2Rad;

                while (i < end)
                {
                    var posX = halfWidth + (Math.Cos(i) * halfWidth);
                    var posY = halfHeight + (Math.Sin(i) * halfHeight);
                    graphics.DrawLine(Pens.Black, halfWidth, halfHeight, (int)posX, (int)posY);
                    this.image.DrawLine(halfWidth, halfHeight, (int)posX, (int)posY, Color.Black);

                    i += stepSize;
                }
            }

            this.GenerateComparisons();


            this.ComparePixels();
        }
    }
}
