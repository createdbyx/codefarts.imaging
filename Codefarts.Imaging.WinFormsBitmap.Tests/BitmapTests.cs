﻿
namespace Codefarts.Imaging.WinFormsBitmap.Tests
{
    using System.Drawing;
    using System.Drawing.Imaging;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Color = Codefarts.Imaging.Color;

    [TestClass]
    public class BitmapTests
    {
        [TestMethod]
        public void GenericImage_ToBitmap()
        {
            var image = new GenericImage<Color>(4, 4);
            var expectedTransparent = System.Drawing.Color.FromArgb(0, 255, 255, 255);
            var expected = System.Drawing.Color.FromArgb(255, 255, 0, 0);
            image.Clear(new Color(expectedTransparent.R, expectedTransparent.G, expectedTransparent.B, expectedTransparent.A));
            image.DrawRectangle(0, 0, 3, 3, new Color(expected.R, expected.G, expected.B, expected.A));

            var bitmap = image.ToBitmap();
            Assert.AreEqual(expected, bitmap.GetPixel(0, 0));
            Assert.AreEqual(expected, bitmap.GetPixel(1, 0));
            Assert.AreEqual(expected, bitmap.GetPixel(2, 0));
            Assert.AreEqual(expected, bitmap.GetPixel(3, 0));

            Assert.AreEqual(expected, bitmap.GetPixel(0, 1));
            Assert.AreEqual(expectedTransparent, bitmap.GetPixel(1, 1));
            Assert.AreEqual(expectedTransparent, bitmap.GetPixel(2, 1));
            Assert.AreEqual(expected, bitmap.GetPixel(3, 1));

            Assert.AreEqual(expected, bitmap.GetPixel(0, 2));
            Assert.AreEqual(expectedTransparent, bitmap.GetPixel(1, 2));
            Assert.AreEqual(expectedTransparent, bitmap.GetPixel(2, 2));
            Assert.AreEqual(expected, bitmap.GetPixel(3, 2));

            Assert.AreEqual(expected, bitmap.GetPixel(0, 3));
            Assert.AreEqual(expected, bitmap.GetPixel(1, 3));
            Assert.AreEqual(expected, bitmap.GetPixel(2, 3));
            Assert.AreEqual(expected, bitmap.GetPixel(3, 3));
        }

        [TestMethod]
        public void Bitmap_ToGenericImage()
        {
            var bitmap = new Bitmap(4, 4, PixelFormat.Format32bppArgb);
            var expectedTransparent = System.Drawing.Color.FromArgb(0, 255, 255, 255);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.Clear(expectedTransparent);
                g.DrawRectangle(Pens.Red, 0, 0, 3, 3);
                g.Flush();
            }

            var image = bitmap.ToGenericImage();
            var transparent = new Color(0, 0, 0, 0);
            Assert.AreEqual(Color.Red, image[0, 0]);
            Assert.AreEqual(Color.Red, image[1, 0]);
            Assert.AreEqual(Color.Red, image[2, 0]);
            Assert.AreEqual(Color.Red, image[3, 0]);

            Assert.AreEqual(Color.Red, image[0, 1]);
            Assert.AreEqual(transparent, image[1, 1]);
            Assert.AreEqual(transparent, image[2, 1]);
            Assert.AreEqual(Color.Red, image[3, 1]);

            Assert.AreEqual(Color.Red, image[0, 2]);
            Assert.AreEqual(transparent, image[1, 2]);
            Assert.AreEqual(transparent, image[2, 2]);
            Assert.AreEqual(Color.Red, image[3, 2]);

            Assert.AreEqual(Color.Red, image[0, 3]);
            Assert.AreEqual(Color.Red, image[1, 3]);
            Assert.AreEqual(Color.Red, image[2, 3]);
            Assert.AreEqual(Color.Red, image[3, 3]);
        }

        [TestMethod]
        public void GenericImage_Draw_Bitmap()
        {
            var bitmap = new Bitmap(4, 4, PixelFormat.Format32bppArgb);
            var expectedTransparent = System.Drawing.Color.FromArgb(0, 255, 255, 255);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.Clear(expectedTransparent);
                g.DrawRectangle(Pens.Red, 0, 0, 3, 3);
                g.Flush();
            }

            var image = new GenericImage<Color>(bitmap.Width, bitmap.Height);
            image.Draw(bitmap, 0, 0);

            var transparent = new Color(0, 0, 0, 0);
            Assert.AreEqual(Color.Red, image[0, 0]);
            Assert.AreEqual(Color.Red, image[1, 0]);
            Assert.AreEqual(Color.Red, image[2, 0]);
            Assert.AreEqual(Color.Red, image[3, 0]);

            Assert.AreEqual(Color.Red, image[0, 1]);
            Assert.AreEqual(transparent, image[1, 1]);
            Assert.AreEqual(transparent, image[2, 1]);
            Assert.AreEqual(Color.Red, image[3, 1]);

            Assert.AreEqual(Color.Red, image[0, 2]);
            Assert.AreEqual(transparent, image[1, 2]);
            Assert.AreEqual(transparent, image[2, 2]);
            Assert.AreEqual(Color.Red, image[3, 2]);

            Assert.AreEqual(Color.Red, image[0, 3]);
            Assert.AreEqual(Color.Red, image[1, 3]);
            Assert.AreEqual(Color.Red, image[2, 3]);
            Assert.AreEqual(Color.Red, image[3, 3]);
        }

        [TestMethod]
        public void Bitmap_Draw_GenericImage()
        {
            var bitmap = new Bitmap(4, 4, PixelFormat.Format32bppArgb);
            var expectedTransparent = System.Drawing.Color.FromArgb(0, 0, 0, 0);
            var expected = System.Drawing.Color.FromArgb(255, 255, 0, 0);

            var image = new GenericImage<Color>(bitmap.Width, bitmap.Height);
            image.Clear(Color.Transparent);
            image.DrawRectangle(0, 0, 3, 3, Color.Red);

            bitmap.Draw(image, 0, 0);

            Assert.AreEqual(expected, bitmap.GetPixel(0, 0));
            Assert.AreEqual(expected, bitmap.GetPixel(1, 0));
            Assert.AreEqual(expected, bitmap.GetPixel(2, 0));
            Assert.AreEqual(expected, bitmap.GetPixel(3, 0));

            Assert.AreEqual(expected, bitmap.GetPixel(0, 1));
            Assert.AreEqual(expectedTransparent, bitmap.GetPixel(1, 1));
            Assert.AreEqual(expectedTransparent, bitmap.GetPixel(2, 1));
            Assert.AreEqual(expected, bitmap.GetPixel(3, 1));

            Assert.AreEqual(expected, bitmap.GetPixel(0, 2));
            Assert.AreEqual(expectedTransparent, bitmap.GetPixel(1, 2));
            Assert.AreEqual(expectedTransparent, bitmap.GetPixel(2, 2));
            Assert.AreEqual(expected, bitmap.GetPixel(3, 2));

            Assert.AreEqual(expected, bitmap.GetPixel(0, 3));
            Assert.AreEqual(expected, bitmap.GetPixel(1, 3));
            Assert.AreEqual(expected, bitmap.GetPixel(2, 3));
            Assert.AreEqual(expected, bitmap.GetPixel(3, 3));
        }
    }
}
