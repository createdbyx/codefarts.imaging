﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Imaging.WinFormsBitmap
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.Drawing.Text;

    /// <summary>
    /// Provides extension methods for the <see cref="Bitmap"/> class.
    /// </summary>
    public static class BitmapExtensionMethods
    {
        /// <summary>
        /// Converts a <see cref="GenericImage{T}"/> to a <see cref="Bitmap"/>.
        /// </summary>   
        /// <param name="image">
        /// The source image to convert to a <see cref="Bitmap"/>.
        /// </param>      
        /// <returns>Returns a new 32bpp Argb <see cref="Bitmap"/> clone of the <see cref="image"/> parameter.</returns>
        /// <exception cref="ArgumentNullException">If the <see cref="image"/> parameter is null.</exception>
        public static Bitmap ToBitmap(this GenericImage<Imaging.Color> image)
        {
            // check for null parameter
            if (image == null)
            {
                throw new ArgumentNullException("image");
            }

            // create the new bitmap with the same dimensions
            var source = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);

            // allocate an array of pixel byte data
            var pixels = new byte[image.Width * image.Height * 4];
            var index = 0;

            // clone the source image and flip it horizontally then vertically so the images will 
            // be oriented the same way
            var clone = image.Clone();
            clone.FlipHorizontally();
            clone.FlipVertically();

            // copy pixel data
            clone.ForEach(
                (genericImage, x, y) =>
                {
                    var color = genericImage[x, y];
                    pixels[pixels.Length - 1 - index++] = color.A;
                    pixels[pixels.Length - 1 - index++] = color.R;
                    pixels[pixels.Length - 1 - index++] = color.G;
                    pixels[pixels.Length - 1 - index++] = color.B;
                });

            // set the source pixels
            var lockedImage = new LockBitmap(source);
            lockedImage.LockBits();
            lockedImage.Pixels = pixels;
            lockedImage.UnlockBits();

            // return the new bitmap
            return source;
        }

        /// <summary>
        /// Converts a <see cref="Bitmap"/> to a <see cref="GenericImage{T}"/>.
        /// </summary>    
        /// <param name="source">
        /// The source image.
        /// </param>     
        /// <returns>Returns a <see cref="GenericImage{T}"/> clone of the <see cref="source"/>.</returns>     
        /// <exception cref="ArgumentNullException">If the <see cref="source"/> parameter is null.</exception>
        public static GenericImage<Imaging.Color> ToGenericImage(this Bitmap source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            // draw source into a new 32bpp argb bitmap so we can use one method for copying pixels
            // this is not ideal and potentially very memory intensive given a image size but for simplicity's sake 
            // this is how it's done
            var bitmap = new Bitmap(source.Width, source.Height, PixelFormat.Format32bppArgb);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.PixelOffsetMode= PixelOffsetMode.None;
                g.CompositingMode = CompositingMode.SourceCopy;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                g.SmoothingMode = SmoothingMode.None;
                g.DrawImage(source, 0, 0);
                g.Flush();
            }
              
            // copy source data to temp image
            var temporaryImage = new GenericImage<Imaging.Color>(bitmap.Width, bitmap.Height);

            // set the source pixels
            var lockedImage = new LockBitmap(bitmap);
            lockedImage.LockBits();
            var pixels = new byte[lockedImage.Pixels.Length];
            lockedImage.Pixels.CopyTo(pixels, 0);
            lockedImage.UnlockBits();

            // copy source pixels
            var index = 0;
            temporaryImage.ForEach(
                (genericImage, indexX, indexY) =>
                {
                    var color = new Imaging.Color();
                    color.A = pixels[pixels.Length - 1 - index++];
                    color.R = pixels[pixels.Length - 1 - index++];
                    color.G = pixels[pixels.Length - 1 - index++];
                    color.B = pixels[pixels.Length - 1 - index++];
                    genericImage[indexX, indexY] = color;
                });

            // flip vertically then horizontally before returning so that images have the same alignment
            temporaryImage.FlipVertically();
            temporaryImage.FlipHorizontally();
            return temporaryImage;
        }

        /// <summary>
        /// Draws a image within another image.
        /// </summary>    
        /// <param name="source">
        /// The source image.
        /// </param>
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x position in the destination image.
        /// </param>
        /// <param name="y">
        /// The y position in the destination image.
        /// </param>
        public static void Draw(this Bitmap source, GenericImage<Imaging.Color> image, int x, int y)
        {
            // there may be more optimized way of doing this but for simplicity's sake 
            // we are going to clone the generic image into a bitmap
            var bitmap = image.ToBitmap();

            // now draw the image into the source
            using (var g = Graphics.FromImage(source))
            {
                g.InterpolationMode = InterpolationMode.NearestNeighbor;
                g.SmoothingMode = SmoothingMode.None;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.None;
                g.PageUnit = GraphicsUnit.Pixel;
                g.TextRenderingHint = TextRenderingHint.SingleBitPerPixel;

                g.DrawImage(bitmap, 0, 0);
                g.Flush();
            }
        }

        /// <summary>
        /// Draws a image within another image.
        /// </summary>    
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="source">
        /// The source image.
        /// </param>
        /// <param name="x">
        /// The x position in the destination image.
        /// </param>
        /// <param name="y">
        /// The y position in the destination image.
        /// </param>
        public static void Draw(this GenericImage<Imaging.Color> image, Bitmap source, int x, int y)
        {
            Draw(image, source, x, y, source.Width, source.Height, 0, 0, source.Width, source.Height, (destinationColor, sourceColor) => sourceColor);
        }

        /// <summary>
        /// Draws a image within another image.
        /// </summary>    
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="source">
        /// The source image.
        /// </param>
        /// <param name="x">
        /// The x position in the destination image.
        /// </param>
        /// <param name="y">
        /// The y position in the destination image.
        /// </param>
        /// <param name="width">
        /// The destination width of the drawn image.
        /// </param>
        /// <param name="height">
        /// The destination height of the drawn image.
        /// </param>
        /// <param name="sourceX">
        /// The x position in the source image.
        /// </param>
        /// <param name="sourceY">
        /// The y position in the source image.
        /// </param>
        /// <param name="sourceWidth">
        /// The source width.
        /// </param>
        /// <param name="sourceHeight">
        /// The source height.
        /// </param>
        /// <param name="blendCallback">
        /// A <see cref="Func{TResult}"/> callback that is used to perform pixel blending.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <see cref="source"/> or <see cref="blendCallback"/> is null.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// If width, height, sourceWidth or sourceHeight are less then 1.
        /// </exception>                                       
        public static void Draw(this GenericImage<Imaging.Color> image, Bitmap source, int x, int y, int width, int height, int sourceX, int sourceY, int sourceWidth, int sourceHeight, Func<Imaging.Color, Imaging.Color, Imaging.Color> blendCallback)
        {
            // perform input validation
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (blendCallback == null)
            {
                throw new ArgumentNullException("blendCallback");
            }

            if (sourceWidth < 1)
            {
                throw new ArgumentOutOfRangeException("sourceWidth");
            }

            if (sourceHeight < 1)
            {
                throw new ArgumentOutOfRangeException("sourceHeight");
            }

            if (width < 1)
            {
                throw new ArgumentOutOfRangeException("width");
            }

            if (height < 1)
            {
                throw new ArgumentOutOfRangeException("height");
            }

            // determine the scale
            var scaledWidth = (float)width / sourceWidth;
            var scaledHeight = (float)height / sourceHeight;

            // optimization check so see if position is within the destination image size
            if (x > image.Width - 1 || y > image.Height - 1 || sourceX > source.Width - 1 || sourceY > source.Height - 1)
            {
                return;
            }

            // copy source data to temp image
            var temporaryImage = source.ToGenericImage();

            var scaled = temporaryImage.Scale(scaledWidth, scaledHeight);

            // optimization check so see if position is still within the destination image size
            if (x < -scaled.Width - 1 || y < -scaled.Height - 1)
            {
                return;
            }

            for (var indexY = 0; indexY < scaled.Height; indexY++)
            {
                for (var indexX = 0; indexX < scaled.Width; indexX++)
                {
                    var destinationX = indexX + x;
                    var destinationY = indexY + y;

                    // TODO: this needs to be better optimized. We are processing all pixels even though some of 
                    // the pixels could be cropped off. Need to calculate the rectangle the copy pixels and process only that rectangle
                    if (destinationX > image.Width - 1 || destinationY > image.Height - 1 || destinationX < 0 || destinationY < 0)
                    {
                        continue;
                    }

                    var value = scaled[indexX, indexY];
                    image[destinationX, destinationY] = blendCallback(image[destinationX, destinationY], value);
                }
            }
        }
    }
}
