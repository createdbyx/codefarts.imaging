﻿// -----------------------------------------------------------------------
// <copyright file="LockBitmap.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Codefarts.Imaging.WinFormsBitmap
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Provides a helper class for gaining access to the pixel data of a <see cref="Bitmap"/> type.
    /// </summary>
    public class LockBitmap
    {
        private Bitmap source;
        private IntPtr Iptr = IntPtr.Zero;
        private BitmapData bitmapData;

        public byte[] Pixels { get; set; }
        public int Depth { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LockBitmap"/> class.
        /// </summary>
        /// <param name="source">
        /// The source bitmap to retrieve and store pixel data from.
        /// </param>
        public LockBitmap(Bitmap source)
        {
            this.source = source;
        }

        /// <summary>
        /// Lock bitmap pixel data.
        /// </summary>
        public void LockBits()
        {
            try
            {
                // Get width and height of bitmap
                Width = source.Width;
                Height = source.Height;

                // get total locked pixels count
                int PixelCount = Width * Height;

                // Create rectangle to lock
                Rectangle rect = new Rectangle(0, 0, Width, Height);

                // get source bitmap pixel format size
                Depth = System.Drawing.Bitmap.GetPixelFormatSize(source.PixelFormat);

                // Check if bpp (Bits Per Pixel) is 8, 24, or 32
                if (Depth != 8 && Depth != 24 && Depth != 32)
                {
                    throw new ArgumentException("Only 8, 24 and 32 bpp images are supported.");
                }

                // Lock bitmap and return bitmap data
                bitmapData = source.LockBits(rect, ImageLockMode.ReadWrite,
                                             source.PixelFormat);

                // create byte array to copy pixel values
                int step = Depth / 8;
                Pixels = new byte[PixelCount * step];
                Iptr = bitmapData.Scan0;

                // Copy data from pointer to array
                Marshal.Copy(Iptr, Pixels, 0, Pixels.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Unlock bitmap data
        /// </summary>
        public void UnlockBits()
        {
            try
            {
                // Copy data from byte array to pointer
                Marshal.Copy(Pixels, 0, Iptr, Pixels.Length);

                // Unlock bitmap data
                source.UnlockBits(bitmapData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get the color of the specified pixel
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>Returns a <see cref="Color"/> representing the pixel color.</returns>
        public Color GetPixel(int x, int y)
        {
            var clr = Color.Empty;

            // Get color components count
            var cCount = this.Depth / 8;

            // Get start index of the specified pixel
            var i = ((y * Width) + x) * cCount;

            if (i > Pixels.Length - cCount)
            {
                throw new IndexOutOfRangeException();
            }

            // For 32 bpp get Red, Green, Blue and Alpha
            if (this.Depth == 32)
            {
                var b = this.Pixels[i];
                var g = this.Pixels[i + 1];
                var r = this.Pixels[i + 2];
                var a = this.Pixels[i + 3]; // a
                clr = Color.FromArgb(a, r, g, b);
            }

            // For 24 bpp get Red, Green and Blue
            if (this.Depth == 24)
            {
                var b = this.Pixels[i];
                var g = this.Pixels[i + 1];
                var r = this.Pixels[i + 2];
                clr = Color.FromArgb(r, g, b);
            }

            // For 8 bpp get color value (Red, Green and Blue values are the same)
            if (this.Depth == 8)
            {
                var c = this.Pixels[i];
                clr = Color.FromArgb(c, c, c);
            }

            return clr;
        }

        /// <summary>
        /// Set the color of the specified pixel
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="color"></param>
        public void SetPixel(int x, int y, Color color)
        {
            // Get color components count
            var cCount = this.Depth / 8;

            // Get start index of the specified pixel
            var i = ((y * this.Width) + x) * cCount;

            // For 32 bpp set Red, Green, Blue and Alpha
            if (this.Depth == 32)
            {
                this.Pixels[i] = color.B;
                this.Pixels[i + 1] = color.G;
                this.Pixels[i + 2] = color.R;
                this.Pixels[i + 3] = color.A;
            }

            // For 24 bpp set Red, Green and Blue
            if (this.Depth == 24)
            {
                this.Pixels[i] = color.B;
                this.Pixels[i + 1] = color.G;
                this.Pixels[i + 2] = color.R;
            }

            // For 8 bpp set color value (Red, Green and Blue values are the same)
            if (this.Depth == 8)
            {
                this.Pixels[i] = color.B;
            }
        }
    }
}
