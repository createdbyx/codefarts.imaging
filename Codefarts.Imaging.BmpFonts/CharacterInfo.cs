﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Imaging.BmpFonts
{
    public struct CharacterInfo
    {
        public char Character;
        public int Index;
        public int X;
        public int Y;
        public int Width;
        public int Height;


         public CharacterInfo(int left, int top, int width, int height)
         {
             this.Index = 0;
             this.Character = default(char);
            this.X = left;
            this.Y = top;
            this.Width = width;
            this.Height = height;
        }
        
        public CharacterInfo(int index, int left, int top, int width, int height)
        {
            this.Character = default(char);
            this.Index = index;
            this.X = left;
            this.Y = top;
            this.Width = width;
            this.Height = height;
        }

        public CharacterInfo(char character,int index, int left, int top, int width, int height)
        {
            this.Character = character;
            this.Index = index;
            this.X = left;
            this.Y = top;
            this.Width = width;
            this.Height = height;
        }
    }
}
