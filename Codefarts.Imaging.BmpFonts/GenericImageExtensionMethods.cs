﻿/*
<copyright>
  Copyright (c) 2013 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.Imaging.BmpFonts
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// Provides extension methods for <see cref="Font"/> and <see cref="GenericImage{T}"/> types.
    /// </summary>
    public static class GenericImageExtensionMethods
    {
        public static void DrawString(this GenericImage<Color> image, Font font, int x, int y, string text)
        {
            throw new NotImplementedException();
        }

        public static void Save(this Font font, Stream stream)
        {
            var doc = font.ToXml();
            using (var writer = XmlWriter.Create(stream, new XmlWriterSettings() { CloseOutput = true, Indent = true }))
            {
                doc.WriteTo(writer);
            }
        }

        /// <summary>
        /// Takes <see cref="Font"/> data and converts it into a <see cref="XDocument"/> type containing xml data describing the font.
        /// </summary>
        /// <param name="font">The source font to get the information from.</param>
        /// <returns>Returns a <see cref="XDocument"/> type containing xml data describing the font.</returns>
        public static XDocument ToXml(this Font font)
        {
            var images = new List<XElement>();

            var i = 0;
            var sb = new StringBuilder();

            foreach (var image in font.Images)
            {                                    
                images.Add(
                    new XElement(
                        "image",
                        new XAttribute("index", i++),
                        new XAttribute("width", image.Width),
                        new XAttribute("height", image.Height),

                new XCData(string.Join(",", image.PixelGrid.Select(x => x.Pack().ToString()).ToArray()))));

                sb.Remove(0, sb.Length);
            }

            var doc = new XDocument(
              new XDeclaration("1.0", "utf-8", "true"),
              new XElement("bmpfont",
                    new XElement("startindex", font.StartIndex),
                    new XElement("endindex", font.EndIndex),
                    new XElement("lineheight", font.Characters.Max(x => x.Height)),
                    font.Characters.Select(x =>
                        new XElement("character",
                            new XAttribute("index", x.Index),
                            new XAttribute("char", (int)x.Character),
                            new XAttribute("x", x.X),
                            new XAttribute("y", x.Y),
                            new XAttribute("width", x.Width),
                            new XAttribute("height", x.Height)
                        )),
                    images.ToArray()));

            return doc;
        }
    }
}
