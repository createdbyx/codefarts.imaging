﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Imaging.BmpFonts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Font
    {
        private Dictionary<char, CharacterInfo> characters;
        private List<GenericImage<Color>> images;
        private int startIndex;
        private int endIndex;
        private int lineHeight;


        public int EndIndex
        {
            get
            {
                return this.endIndex;
            }

            private set
            {
                this.endIndex = value;
            }
        }

        public int StartIndex
        {
            get
            {
                return this.startIndex;
            }

            private set
            {
                this.startIndex = value;
            }
        }

        public IEnumerable<CharacterInfo> Characters
        {
            get
            {
                foreach (var info in this.characters)
                {
                    yield return info.Value;
                }
            }
        }

        public IEnumerable<GenericImage<Color>> Images
        {
            get
            {
                foreach (var image in this.images)
                {
                    yield return image;
                }
            }
        }

        public Font()
        {
            this.characters = new Dictionary<char, CharacterInfo>();
            this.images = new List<GenericImage<Color>>();
        }

        public Font(IEnumerable<CharacterInfo> characters, IList<GenericImage<Color>> images)
            : this()
        {
            if (characters == null)
            {
                throw new ArgumentNullException("characters");
            }

            if (this.images == null)
            {
                throw new ArgumentNullException("images");
            }

            this.images.AddRange(images);

            foreach (var info in characters)
            {
                if (this.characters.ContainsKey(info.Character))
                {
                    this.characters[info.Character] = new CharacterInfo(info.X, info.Y, info.Width, info.Height) { Character = info.Character };
                }
                else
                {
                    this.characters.Add(info.Character, new CharacterInfo(info.X, info.Y, info.Width, info.Height) { Character = info.Character });
                }

                if (this.startIndex > info.Character)
                {
                    this.startIndex = info.Character;
                }

                if (this.endIndex < info.Character)
                {
                    this.endIndex = info.Character;
                }
            }

            this.lineHeight = this.characters.Max(x => x.Value.Height);
        }

        public int LineHeight
        {
            get
            {
                return this.lineHeight;
            }
        }
    }
}
