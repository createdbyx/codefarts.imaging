﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Imaging.BmpFonts
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// Provides a helper class for loading a bitmap <see cref="Font"/>.
    /// </summary>
    public class Helpers
    {
        /// <summary>
        /// Loads data from a stream and constructs a <see cref="Font"/> type from it.
        /// </summary>
        /// <param name="stream">The stream containing the font data.</param>
        /// <param name="images">A list of images containing the font characters.</param>
        /// <returns></returns>
        public static Font Load(Stream stream, IList<GenericImage<Color>> images)
        {
            // attempt to load the stream as a xml file
            var doc = XDocument.Load(stream);
            if (doc.Root == null)
            {
                throw new XmlException("Xml data does not appear to contain a root element.");
            }

            // check to see if root name is bmpfont
            if (doc.Root.Name.LocalName.ToLower() != "bmpfont")
            {
                throw new XmlException("Root element is not 'bmpfont'");
            }

            // parse character data
            var characters = from item in doc.Root.Elements("character")
                             select new CharacterInfo(
                                 int.Parse(item.Attribute("x").Value),
                                 int.Parse(item.Attribute("y").Value),
                                 int.Parse(item.Attribute("width").Value),
                                 int.Parse(item.Attribute("height").Value))
                                    {
                                     Index = int.Parse(item.Attribute("index").Value), 
                                     Character = (char)int.Parse(item.Attribute("character").Value)
                                    };

            // return the new font
            return new Font(characters, images);
        }            
    }
}
