namespace Codefarts.Imaging
{
    using System;

    /// <summary>
    /// Contains extension methods for the <see cref="GenericImage{T}"/> type.
    /// </summary>
    public static partial class GenericImageExtensionMethods
    {
        /// <summary>
        /// Builds a comparison image.
        /// </summary>
        /// <param name="image">
        /// The source image.
        /// </param>
        /// <param name="imageToCompare">
        /// A reference image to compare against.
        /// </param>
        /// <param name="relativeValues">
        /// If True will return relative color values, otherwise will return a black image with white pixels representing the differences.
        /// </param>
        /// <remarks>
        /// Compares two images and return the differences between them. Will use Black for the color match color.
        /// </remarks>
        public static GenericImage<Color> BuildAlphaComparison<T>(
            this GenericImage<Color> image,
            GenericImage<Color> imageToCompare,
            bool relativeValues)
        {
            return BuildAlphaComparison<Color>(image, imageToCompare, relativeValues, Color.Black);
        }

        /// <summary>
        /// Builds a comparison image.
        /// </summary>
        /// <param name="image">
        /// The source image.
        /// </param>
        /// <param name="imageToCompare">
        /// A reference image to compare against.
        /// </param>
        /// <param name="relativeValues">
        /// If True will return relative color values, otherwise will return a black image with white pixels representing the differences.
        /// </param>
        /// <remarks>
        /// Compares two images and return the differences between them.
        /// </remarks>
        public static GenericImage<Color> BuildAlphaComparison<T>(this GenericImage<Color> image, GenericImage<Color> imageToCompare, bool relativeValues, Color colorMatchColor)
        {
            if (image.Width != imageToCompare.Width)
            {
                throw new Exception("Image widths do not match.");
            }

            if (image.Height != imageToCompare.Height)
            {
                throw new Exception("Image heights do not match.");
            }

            var newImage = new GenericImage<Color>(image.Width, image.Height);
            for (var y = 0; y < image.Height; y++)
            {
                for (var x = 0; x < image.Width; x++)
                {
                    var colorA = image[x, y];
                    var colorB = imageToCompare[x, y];

                    if (relativeValues)
                    {
                        newImage[x, y] = new Color(
                            (byte)Math.Abs(colorB.R - colorA.R),
                            (byte)Math.Abs(colorB.G - colorA.G),
                            (byte)Math.Abs(colorB.B - colorA.B),
                            (byte)Math.Abs(colorB.A - colorA.A));
                    }
                    else
                    {
                        newImage[x, y] = colorA != colorB ? Color.White : colorMatchColor;
                    }
                }
            }

            return newImage;
        }
    }
}