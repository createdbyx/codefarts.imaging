﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.Imaging
{
    /// <summary>
    /// Provides public constant values.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Degrees-to-radians conversion constant.
        /// </summary>
        public const float Deg2Rad = 0.01745329f;

        /// <summary>
        /// Radians-to-degrees conversion constant.
        /// </summary>
        public const float Rad2Deg = 57.29578f;

        /// <summary>
        /// A tiny floating point value.
        /// </summary>
        public const float Epsilon = 1.401298E-45f;
    }
}