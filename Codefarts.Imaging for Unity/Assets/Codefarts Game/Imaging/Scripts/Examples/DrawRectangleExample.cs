﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.Imaging.Scripts
{
    using System;

    using UnityEngine;

    using Color = Codefarts.Imaging.Color;

    /// <summary>
    /// Provides an example for drawing rectangles.
    /// </summary>
    internal class DrawRectangleExample : BaseExample
    {
        private bool animated;

        private bool filled;

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawRectangleExample"/> class.
        /// </summary>
        public DrawRectangleExample()
        {
            this.image = new GenericImage<Color>(256, 256);
            this.image.Clear(Color.White);
            if (this.filled)
            {
                this.image.FillRectangle(0, 0, this.image.Width - 1, this.image.Height - 1, Color.Red);
            }
            else
            {
                this.image.DrawRectangle(0, 0, this.image.Width - 1, this.image.Height - 1, Color.Red);
            }

            this.IsDirty = true;
        }

        /// <summary>
        /// Gets or sets a <see cref="GenericImage{T}"/> containing the image result for the example.
        /// </summary>
        public override GenericImage<Color> Image
        {
            get
            {
                // check if settings have changed
                if (this.changed)
                {
                    this.image.Clear(Color.White);
                    if (this.animated)
                    {
                        var value = (float)(Math.Sin(Time.time) + 1) * (this.image.Width / 2f);
                        if (this.filled)
                        {
                            this.image.FillRectangle((this.image.Width / 2) - (int)(value / 2f), (this.image.Height / 2) - (int)(value / 2f), (int)value, (int)value, Color.Red);
                        }
                        else
                        {
                            this.image.DrawRectangle((this.image.Width / 2) - (int)(value / 2f), (this.image.Height / 2) - (int)(value / 2f), (int)value, (int)value, Color.Red);
                        }
                    }
                    else
                    {
                        if (this.filled)
                        {
                            this.image.FillRectangle(0, 0, this.image.Width - 1, this.image.Height - 1, Color.Red);
                        }
                        else
                        {
                            this.image.DrawRectangle(0, 0, this.image.Width - 1, this.image.Height - 1, Color.Red);
                        }
                    }

                    // set changed to false
                    this.changed = false;
                }

                if (this.animated)
                {
                    this.IsDirty = true;
                }

                return this.image;
            }

            set
            {

            }
        }

        /// <summary>
        /// Gets the title of the example.
        /// </summary>
        public override string Title
        {
            get
            {
                return "Draw Rectangle";
            }
        }

        /// <summary>
        /// Draws the setting controls.
        /// </summary>
        public override void DrawSettings()
        {
            var value = GUILayout.Toggle(this.animated, "Animated");
            if (value != this.animated)
            {
                this.IsDirty = true;
                this.animated = value;
            }

            value = GUILayout.Toggle(this.filled, "Filled");
            if (value != this.filled)
            {
                this.IsDirty = true;
                this.filled = value;
            }
        }
    }
}