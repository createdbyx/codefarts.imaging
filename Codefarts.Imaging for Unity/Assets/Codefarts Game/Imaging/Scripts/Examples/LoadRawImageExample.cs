﻿namespace Codefarts.Imaging.Scripts
{
    using System;
    using System.IO;

    using UnityEngine;

    using Color = Codefarts.Imaging.Color;
    using Random = System.Random;

    /// <summary>
    /// Provides an example for drawing rectangles.
    /// </summary>
    internal class LoadRawImageExample : BaseExample
    {
        private string filePath;

        private bool filled;

        private string fileName = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadRawImageExample"/> class.
        /// </summary>
        public LoadRawImageExample()
        {
            this.IsDirty = true;
            this.image = new GenericImage<Color>(256, 256);
        }

        /// <summary>
        /// Gets or sets a <see cref="GenericImage{T}"/> containing the image result for the example.
        /// </summary>
        public override GenericImage<Color> Image
        {
            get
            {
                // check if settings have changed
                if (this.changed)
                {
                    using (var stream = File.OpenRead(this.fileName))
                    {
                        this.image.Load32BppRaw(stream);
                    }

                    // set changed to false
                    this.changed = false;
                }

                return this.image;
            }

            set
            {

            }
        }

        /// <summary>
        /// Gets the title of the example.
        /// </summary>
        public override string Title
        {
            get
            {
                return "Load Raw";
            }
        }

        /// <summary>
        /// Draws the setting controls.
        /// </summary>
        public override void DrawSettings()
        {
            this.fileName = GUILayout.TextField(this.fileName, "File name");

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Load"))
            {
                this.IsDirty = true;
            }

            if (GUILayout.Button("Save"))
            {
                this.IsDirty = true;
            }

            GUILayout.EndHorizontal();

            if (GUILayout.Button("Generate"))
            {
                for (var i = 0; i < 10; i++)
                {
                    var rnd = new Random((int)DateTime.Now.Ticks);
                    this.image.FillCircle(rnd.Next(0, this.image.Width), rnd.Next(0, this.image.Height), rnd.Next(5, this.image.Width / 4),
                        new Color((byte)rnd.Next(255), (byte)rnd.Next(255), (byte)rnd.Next(255), 255));
                }
            }
        }
    }
}