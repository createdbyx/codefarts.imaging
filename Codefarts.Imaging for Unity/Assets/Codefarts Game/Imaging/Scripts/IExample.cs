﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.Imaging.Scripts
{
    /// <summary>
    /// Provides a interface for examples.
    /// </summary>
    public interface IExample
    {
        /// <summary>
        /// Gets or sets a <see cref="GenericImage{T}"/> containing the image result for the example.
        /// </summary>
        GenericImage<Color> Image { get; set; }

        /// <summary>
        /// Gets ro sets a value indicating whether or not the example is dirty.
        /// </summary>
        bool IsDirty { get; set; }

        /// <summary>
        /// Gets the title of the example.
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Draws the setting controls.
        /// </summary>
        void DrawSettings();  
    }
}