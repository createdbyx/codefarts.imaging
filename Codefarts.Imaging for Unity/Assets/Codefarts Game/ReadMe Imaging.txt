﻿Copyright (c) 2012 Codefarts 
contact@codefarts.com
http://www.codefarts.com/
All rights reserved.

The generic imaging library is a portable class library written in pure C# designed to provide generic bitmap manipulation 
capabilities without any reliance on external dependencies.

The driving force behind this project is to have a platform independent library that is written in type safe pure C#. 
I can't tell you how many times I have wanted a easy to use imaging library that I could use across any platform & framework.

Silverlight/WPF, GDI, WinForms, XNA/DirectX and every other library out there takes there own approach to working with 
bitmap images. Forcing you to write code that is not platform/framework independent. This creates way too much unnecessary coding.

Documentation is available in the documentation folder "Documentation\ImagingDocumentation.zip". 
 